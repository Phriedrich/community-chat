var stompClient = null;
        
var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

/**
 * Connects the Websession via SockJS and Stomp to the Websocket and subscribes with the refreshMessages Method to the /topic/newMessage Destination.
 */
function connect() {
    var socket = new SockJS('/newMessage');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        stompClient.subscribe('/topic/newMessage', function(message){
            refreshMessages(JSON.parse(JSON.parse(message.body).content));
        });
    });
}

/**
 * Disconnects from the stompClient
 */
function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
}

/**
 * Refreshes the messages and builds the Message List
 */
function refreshMessages(messages) {
    $(".media-list").html("");
    var messagesString = "";
    $.each(messages.reverse(), function(i, message) {
    	var color = getAvatarColor(message);
    	var date = formatDate(new Date(message.createDate));
    	if(messagesString.length > 0){
    		messagesString += '<hr/>';
    	}
    	messagesString +='<li class="media"><i class="avatar" style="background-color:' + color +  '">' + message.author[0] + '</i><div class="media-body"><div class="media"><div class="media-body" style="white-space: pre-line">'
        + message.text + '<br/><small class="text-muted">' + message.author + ' | ' + date + '</small></div></div></div></li>';
    });
    $(".media-list").append(messagesString);
    $('.media-list').scrollTop($('.media-list')[0].scrollHeight);
    // Scroll Down after Refresh / new Message
    scrollDown();
   
}

/**
 * Builds from the Form and Partner data a ChatMessage JSON String and sends it, using the stompClient, to the /app/newMessage destination
 * @param partnerNummer
 * @param partnerName
 */
function sendMessage(partnerNummer, partnerName) {
    $container = $('.media-list');
    $container[0].scrollTop = $container[0].scrollHeight;
    var message = $("#messageText").val();
    if(message !== null && message.length > 0){
    	// replaces all no HTML Chars and Sequences with html replacements
        message = message.replace(/\r\n|\r|\n/g,"<br/>");
        message = message.replace(/\\/g, "&#92;");
        message = message.replace(/'/g, "&apos;");
        message = message.replace(/"/g, "&quot;");
        // if no partnerName is given the author is set to the cookie set on the login Page
        if(partnerName !== null){
        	var author = partnerName;
        } else {
            var author = $.cookie("realtime-chat-nickname");
        }

        stompClient.send("/app/newMessage", {}, JSON.stringify({ 'text': message, 'author': author, "partnerNummer": partnerNummer, "partnerName": partnerName}));

        $("#messageText").val("")
        $container.animate({ scrollTop: $container[0].scrollHeight }, "slow");
    }
}

/**
 * Scrolls the ChatList down
 */
function scrollDown(){
	document.getElementById('chatDiv').scrollTop = document.getElementById('chatDiv').scrollHeight - document.getElementById('chatDiv').offsetHeight;
}

/**
 * returns an Avatar Color Code by using the hash of the author value
 * @param message
 * @returns colorcode
 */
function getAvatarColor(message) {
	var messageSender = message.author;
	var isSupport = message.isSupport;
	if(typeof isSupport !== 'undefined' && isSupport !== null && isSupport !== "null"){
		return "#347E87";
	}
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

/**
 * Returns the date in a custom format
 * @param date
 * @returns dateString
 */
function formatDate(date) {
	  var day = date.getDate();
	  var month = date.getMonth();
	  var year = date.getFullYear();
	  var hour = date.getHours();
	  var minute = date.getMinutes();
	  return (day<10?'0':'') + day + '/' + (month + 1<10?'0':'') + (month + 1) + '/' + year + ' ' + (hour<10?'0':'') + hour + ':' + (minute<10?'0':'') + minute ;
}

/**
 * Set style Property for mobile viewport. (As adjustment to the wrong viewport interpretation, on the basis of the dynamic mobile searchbars) 
 */
function setViewportPropertyForMobile(){
	var vh = window.innerHeight * 0.01;
   	document.documentElement.style.setProperty('--vh', `${vh}px`);
}

