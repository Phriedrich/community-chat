package de.synaxon.communitychat.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.synaxon.communitychat.domain.ChatMessageModel;

import java.util.List;


public interface ChatMessageRepository extends MongoRepository<ChatMessageModel, String> {
    List<ChatMessageModel> findAllByOrderByCreateDateAsc();
}
