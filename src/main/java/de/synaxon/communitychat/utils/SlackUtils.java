package de.synaxon.communitychat.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.synaxon.communitychat.domain.ChatMessageModel;
/**
 * Class for Slack Utility Methods for the slack api
 * @author Christian Bauer
 * @created 07.03.2019
 */
@Component
public class SlackUtils {
	
	public final static String TEXT = "text";
	public final static String TRIGGER = "trigger_word";
	
	@Value("#{supportName}")
	public final static String SUPPORT_USER = "EGIS Support";
	
	private static final String POST = "POST";
	private static final String PAYLOAD = "payload=";
	private static final String UTF_8 = "UTF-8";

	/**
	 * Converts the given ChatMessage to json and sends it to the given slack Webhook
	 * @param message
	 * @param webhookUrl
	 */
	public void sendChatMessageToSlack(ChatMessageModel message, String webhookUrl) {
		String jsonString = chatMessageToSlackJson(message);
		sendJsonToSlack(jsonString, webhookUrl);
	}
	
	/**
	 * Sends a json String to the given slack WebHook
	 * @param jsonString
	 * @param webhookUrl
	 */
	public void sendJsonToSlack(String jsonString, String webhookUrl) {
		HttpURLConnection connection = null;
		try {			
			// Create connection
			final URL url = new URL(webhookUrl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(POST);
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			final String payload = PAYLOAD + URLEncoder.encode(jsonString, UTF_8);

			// Send request
			final DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(payload);
			wr.flush();
			wr.close();

			// Get Response
			final InputStream is = connection.getInputStream();
			final BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuilder response = new StringBuilder();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\n');
			}

			rd.close();
		} catch (Exception e) {
			System.out.println("Exception posting to Slack " + e);
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
	
	/**
	 * Converts a ChatMessage to JSON readable by slack
	 * @param message
	 * @return JSONString
	 */
	private String chatMessageToSlackJson(ChatMessageModel message) {
		if(message.getText() != null && message.getText().length() > 0) {
			String finalText = "";
			String text = HtmlUtils.htmlUnescape(message.getText());
			// replaces the HTML Linebreak with a by slack readable linebreak.
			text = text.replace("<br/>", "\n");
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
	        String date = formatter.format(message.getCreateDate());
			if(message.getPartnerName() != null) {
				if(message.getPartnerNummer() != null) {
					finalText = "*" + message.getPartnerName() + " (" + message.getPartnerNummer() + ") - " + date + "*\n";
				} else {
					finalText = "*" + message.getPartnerName() + " - " + date + "*\n";
				}
			} else if (message.getAuthor() != null) {
				finalText = "*" + message.getAuthor() + " - " + date + "*\n";
			}
			finalText += text;
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.createObjectNode();
			((ObjectNode) rootNode).put("text", finalText);
			try {
				return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
				
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Parses the Payload to a new ChatMessage and returns it
	 * @param payload
	 * @return ChatMessageModel
	 */
	public static ChatMessageModel parseSlackPayloadToChatMessage(Map<String, String> payload) {
		String text = payload.get(SlackUtils.TEXT);
    	String trigger = payload.get(SlackUtils.TRIGGER);
    	if(trigger != null && trigger.length() > 0) {
        	text = text.replace(trigger, "");
    	}
    	if(text != null && text.length() > 0) {
    		 // replaces all no HTML Tags and Chars with HTML Chars
	    	 text = text.trim();
	    	 text = text.replaceAll("(\r\n|\n\r|\r|\n)", "<br />");
	    	 text = text.replace("\\", "&#92;");
	    	 text = text.replace("\'", "&apos;");
	    	 text = text.replace("\"", "&quot;");
	   		 ChatMessageModel chatMessage = new ChatMessageModel(text, SlackUtils.SUPPORT_USER, new Date(), "true");
	   		 return chatMessage;
    	}
    	return null;
	}
	
	
}
