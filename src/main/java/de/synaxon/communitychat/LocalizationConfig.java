/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.synaxon.communitychat;

import java.util.Locale;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
/**
 * @author Christian Bauer
 * @created 06.03.2019
 */
@Configuration
public class LocalizationConfig {
	
    @Value("${service.locale}")
    private String locale;
    
    @Bean
    LocaleResolver localeResolver(){
       FixedLocaleResolver l = new FixedLocaleResolver();
       l.setDefaultLocale(new Locale(locale));
       return l;
    }
}
