package de.synaxon.communitychat.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.synaxon.communitychat.domain.ChatMessageModel;
import de.synaxon.communitychat.message.ChatMessage;
import de.synaxon.communitychat.repository.ChatMessageRepository;
import de.synaxon.communitychat.utils.SlackUtils;
import de.synaxon.logging.EventLogger;

/**
 * @author Christian Bauer
 * @created 04.03.2019
 */
@Controller
public class ChatMessageController {
	
    @Value("${spring.application.name}")
    private String appName;
    
    @Value("${service.slack.webhook}")
    private String slackWebhook;

    @Autowired
    private ChatMessageRepository chatMessageRepository;
    
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;
    
    @Autowired
    SlackUtils slackUtils;
   
   
    
    /**
     * Simple Index Page
     * @return index page
     */
    @RequestMapping(value = { "/", "/index"}, method = RequestMethod.GET)
    public String indexPage(Model model) {
    	model.addAttribute("appName", appName);
    	EventLogger.getInstance().setAction("call").setPageName("index").addParameter("action_source", "direct call").log();
        return "index";
    }

    /**
     * Login Page to set an defined author
     * @return login page
     */
    @RequestMapping("/login")
    public String login(Model model) {
    	model.addAttribute("appName", appName);
    	EventLogger.getInstance().setAction("call").setPageName("login").addParameter("action_source", "direct call").log();
        return "login";
    }
    
    /**
     * Initial Page for the chat function
     * @return chat page
     */
    @RequestMapping("/chat")
    public String chat(@RequestParam(name="partnerNummer",required=false) String partnerNummer,@RequestParam(name="partnerName",required=false) String partnerName, Model model) {
    	EventLogger.getInstance().setAction("call").setPageName("chat").addParameter("action_source", "direct call").log();
    	model.addAttribute("partnerName", partnerName);
    	model.addAttribute("partnerNummer", partnerNummer);
    	model.addAttribute("appName", appName);
    	return "chat";
    }

    /**
     * Post method to create a new message from the handled JSON.
     * The messageMapping binds this function to handle STOMP messages to the defined destination / mapping of the Websocket
     * The sendTo binds the return of this function to the defined destination / mapping
     * @param chatMessageModel
     * @return ChatMessage
     */
    @RequestMapping(value = "/messages", method = RequestMethod.POST)
    @MessageMapping("/newMessage")
    @SendTo("/topic/newMessage")
    public ChatMessage save(ChatMessageModel chatMessageModel) {
        ChatMessageModel chatMessage = new ChatMessageModel(chatMessageModel.getText(), chatMessageModel.getAuthor(), new Date(), chatMessageModel.getPartnerNummer(), chatMessageModel.getPartnerName());
        chatMessageRepository.save(chatMessage);
        List<ChatMessageModel> chatMessageModelList = chatMessageRepository.findAll(PageRequest.of(0, 100, Sort.Direction.DESC, "createDate")).getContent();
        slackUtils.sendChatMessageToSlack(chatMessage, slackWebhook);
        EventLogger.getInstance().setAction("modify_content").setPageName("chat")
        	.addParameter("page_action", "new_message")
        	.addParameter("author", chatMessage.getAuthor())
        	.addParameter("partnerName", chatMessage.getPartnerName())
        	.addParameter("partnerNummer", chatMessage.getPartnerNummer())
        	.addParameter("text", chatMessage.getText())
        	.addParameter("action_source", "click").log();
        return new ChatMessage(chatMessageModelList.toString());
    }

    /**
     * Get method to return the last 100 messages
     * @return HttpEntity ChatMessageList
     */
    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public HttpEntity<List<ChatMessageModel>> list() {
        List<ChatMessageModel> chatMessageModelList = chatMessageRepository.findAll(PageRequest.of(0, 100, Sort.Direction.DESC, "createDate")).getContent();
        return new ResponseEntity<List<ChatMessageModel>>(chatMessageModelList, HttpStatus.OK);
    }
    
    /**
     * Post method to handle slack replies.
     * This method gathers the slack payload and converts the slack message to a ChatMessage.
     * After that the method sends the message to the defined WebSocket destination.
     * @param payload
     * @return ChatMessage
     */
    @RequestMapping(value = "/slack", method = RequestMethod.POST)
    public @ResponseBody ChatMessage slackApi(@RequestParam Map<String, String> payload) {
		if(payload != null) {
	   		 ChatMessageModel chatMessage = SlackUtils.parseSlackPayloadToChatMessage(payload);
	   		 if(chatMessage != null) {
			     chatMessageRepository.save(chatMessage);
		         List<ChatMessageModel> chatMessageModelList = chatMessageRepository.findAll(PageRequest.of(0, 100, Sort.Direction.DESC, "createDate")).getContent();
			     messagingTemplate.convertAndSend("/topic/newMessage", new ChatMessage(chatMessageModelList.toString()));
			     EventLogger.getInstance().setAction("modify_content").setPageName("chat")
		        	.addParameter("page_action", "new_slack_message")
		        	.addParameter("author", chatMessage.getAuthor())
		        	.addParameter("support", chatMessage.getIsSupport())
		        	.addParameter("text", chatMessage.getText())
		        	.addParameter("action_source", "slack_api").log();
			     return new ChatMessage(chatMessageModelList.toString());
	    	}
		}
    	return null;
    }
    

}
